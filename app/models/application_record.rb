# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def bamboo_client
    @bamboo_client ||= Bamboo.new
  end
end
