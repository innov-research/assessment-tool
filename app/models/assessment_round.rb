# frozen_string_literal: true

# == Schema Information
#
# Table name: assessment_rounds
#
#  id                        :bigint           not null, primary key
#  cut_off                   :date
#  department                :string
#  division                  :string
#  due_date                  :date
#  load_previous_assessments :boolean          default(FALSE)
#  promotion_planning        :boolean          default(FALSE)
#  start_date                :date
#  state                     :integer          default("open")
#  succession_planning       :boolean          default(FALSE)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  functional_lead_id        :integer
#  user_id                   :bigint           not null
#
# Indexes
#
#  index_assessment_rounds_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class AssessmentRound < ApplicationRecord
  belongs_to :user
  has_many :assessments, dependent: :destroy

  validates :due_date, :start_date, presence: true
  validate :only_one_open_per_department, :due_date_after_start_date, :start_date_is_not_in_the_past, on: :create
  validate :department_or_division_is_present, on: :create

  STATUSES = [:open, :in_review, :closed, :synced].freeze
  OPEN_STATUSES = [:open, :in_review].freeze
  CLOSED_STATUSES = [:closed, :synced].freeze
  EDITABLE_STATUSES = [:open, :in_review, :closed].freeze

  enum state: STATUSES

  scope :non_closed, -> { where(state: OPEN_STATUSES) }
  scope :closed_and_synced, -> { where(state: CLOSED_STATUSES) }
  scope :not_synced, -> { where.not(state: :synced) }
  scope :for_department, ->(department) { where('department = ?', department) }
  scope :for_division, ->(division) { where('division = ?', division) }

  before_create :set_functional_lead
  after_create :load_assessments, if: :load_previous_assessments?

  def ongoing?
    Date.current >= start_date && open?
  end

  def all_team_members_assessed?
    percentage_complete === 100.0
  end

  def assessed_team_members
    team_members = assessments.pluck(:team_member_bamboo_id).uniq.map do |id|
      bamboo_client.find_team_member_by_id(id)
    end.compact
    team_members.sort_by { |team_member| team_member['fullName1'] }
  end

  def percentage_complete
    all_assessment_ids = assessments.pluck(:team_member_bamboo_id).map(&:to_s).uniq
    all_eligibile_team_member_ids = eligible_team_members.pluck('id')
    (all_assessment_ids.size.to_f / (all_eligibile_team_member_ids.size - 1) * 100).round
  end

  def people_managers
    team_member_ids = bamboo_client.find_team_members_by_department(department).pluck('id') if department
    team_member_ids ||= bamboo_client.find_team_members_by_division(division).pluck('id') if division
    team_member_ids.each_with_object([]) do |id, array|
      array << bamboo_client.find_team_member_by_id(id) if bamboo_client.direct_reports(id).any?
    end
  end

  def move_to_review!
    return unless open?

    self.state = :in_review
    save!
  end

  def close!
    return unless in_review?

    self.state = :closed
    save!
  end

  def sync_to_bamboo!
    latest_confirmed_assessments = Assessment.most_recent_in_round(id).confirmed
    if ENV['SYNC_TO_BAMBOO']
      latest_confirmed_assessments.each do |assessment|
        bamboo_client.add_rating_details(
          assessment.team_member_bamboo_id,
          assessment.score_performance.capitalize,
          "#{assessment.score_potential.capitalize} Potential"
        )
      end
    end

    latest_confirmed_assessments.update(synced_at: DateTime.now)
    self.state = :synced
    save!
  end

  def generate_csv
    reports = eligible_team_members
    latest_assessments = Assessment.most_recent_in_round(id)
    CSV.generate(headers: true) do |csv|
      headers = %w[bamboo_id name department manager score_performance score_potential]
      if succession_planning || promotion_planning
        headers << 'emergency_contact'
        headers << 'time_frame'
        headers << 'time_frame_promotion'
      end
      csv << headers

      latest_assessments.each do |assessment|
        assessment_for = reports.find { |report| report['id'] == assessment.team_member_bamboo_id.to_s }
        # Could be someone was assessed but is no longer active
        next unless assessment_for

        manager = bamboo_client.find_team_member_by_id(assessment_for['supervisorEId'])
        row_data = [assessment.team_member_bamboo_id, assessment_for['fullName1'], assessment_for['department'], manager['fullName1'], assessment.score_performance, assessment.score_potential]
        if succession_planning || promotion_planning
          row_data << assessment.emergency_contact
          row_data << assessment.time_frame
          row_data << assessment.time_frame_promotion
        end

        csv << row_data
      end
    end
  end

  def total_rewards_data
    reports = eligible_team_members
    latest_assessments = Assessment.most_recent_in_round(id)

    latest_assessments.each_with_object([]) do |assessment, data|
      assessment_for = reports.find { |report| report['id'] == assessment.team_member_bamboo_id.to_s }
      next unless assessment_for

      data << [assessment_for['employeeNumber'], assessment_for['fullName1'], assessment.score_performance]
    end
  end

  def set_functional_lead
    return if functional_lead_id # it's already set
    return unless functional_lead

    self.functional_lead_id = functional_lead['id']
  end

  def eligible_team_members
    team_members = bamboo_client.find_team_members_by_department(department) if department
    team_members ||= bamboo_client.find_team_members_by_division(division) if division
    team_members.select! { |team_member| Date.parse(team_member['hireDate']).before?(cut_off) } if cut_off
    team_members
  end

  private

  def functional_lead
    possible_functional_leads = eligible_team_members.select do |tm|
      manager = eligible_team_members.find { |man| man['id'] === tm['supervisorEId'] }
      manager.nil?
    end

    Raven.capture_exception("Can't determine the functional lead correctly for assessment round #{id}") if possible_functional_leads.size > 1
    possible_functional_leads.first
  end

  def only_one_open_per_department
    errors.add(:department, "There can only be one open round/department.") if AssessmentRound.non_closed.where(department: department).exists? && department
  end

  def due_date_after_start_date
    errors.add(:due_date, "Due date needs to be after the start date.") if due_date <= start_date
  end

  def start_date_is_not_in_the_past
    errors.add(:start_date, "Start date can not be in the past.") if start_date < Date.today
  end

  def department_or_division_is_present
    errors.add(:department, 'No department or division is present') if division.nil? && department.nil?
  end

  def load_assessments
    round = AssessmentRound.where.not(id: id).where(department: department).order(created_at: :desc).first if department
    round ||= AssessmentRound.where.not(id: id).where(division: division).order(created_at: :desc).first if division

    round.assessments.each do |assessment|
      dup_assessment = assessment.dup
      dup_assessment.given_by = round.user.id
      dup_assessment.assessment_round = self
      dup_assessment.save
    end
  end
end
