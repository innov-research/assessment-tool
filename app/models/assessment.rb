# frozen_string_literal: true

# == Schema Information
#
# Table name: assessments
#
#  id                    :bigint           not null, primary key
#  confirmed_at          :datetime
#  emergency_contact     :string
#  given_by              :integer
#  motivation            :text
#  note                  :text
#  score_performance     :integer
#  score_potential       :integer
#  synced_at             :datetime
#  time_frame            :string
#  time_frame_promotion  :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  assessment_round_id   :bigint           not null
#  team_member_bamboo_id :integer
#
# Indexes
#
#  index_assessments_on_assessment_round_id  (assessment_round_id)
#
# Foreign Keys
#
#  fk_rails_...  (assessment_round_id => assessment_rounds.id)
#
class Assessment < ApplicationRecord
  belongs_to :user, foreign_key: 'given_by', inverse_of: :assessments
  belongs_to :assessment_round

  validates :given_by, presence: true
  validates :team_member_bamboo_id, presence: true

  PERFORMANCE_VALUES = [:developing, :performing, :exceeding].freeze
  POTENTIAL_VALUES = [:low, :medium, :high].freeze

  enum score_performance: PERFORMANCE_VALUES
  enum score_potential: POTENTIAL_VALUES

  scope :confirmed, -> { where.not(confirmed_at: nil) }
  scope :most_recent, -> { select('DISTINCT ON (team_member_bamboo_id) *').order("team_member_bamboo_id, created_at DESC") }

  # Returns the most recent assessments given to every report
  def self.most_recent_in_round_for(reports, round)
    select('DISTINCT ON (team_member_bamboo_id) *').where(assessment_round: round).where(team_member_bamboo_id: reports.pluck('id')).order("team_member_bamboo_id, created_at DESC")
  end

  def self.most_recent_in_round(round)
    select('DISTINCT ON (team_member_bamboo_id) *').where(assessment_round: round).order("team_member_bamboo_id, created_at DESC")
  end
end
