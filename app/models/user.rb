# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  deactivated            :boolean          default(FALSE)
#  departments            :string           default([]), is an Array
#  divisions              :string           default([]), is an Array
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  failed_attempts        :integer          default(0), not null
#  locked_at              :datetime
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  role                   :integer
#  uid                    :string
#  unlock_token           :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  bamboo_id              :integer
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :timeoutable, :trackable
  devise :database_authenticatable, :rememberable, :validatable,
         :omniauthable, omniauth_providers: [:oktaoauth]

  validates :email, format: { with: /\A[A-Z0-9._%a-z\-]+@gitlab\.com\z/, message: "must be a gitlab.com account" }

  has_many :assessments, foreign_key: 'given_by', class_name: 'Assessment', dependent: :destroy, inverse_of: :user
  has_many :assessment_rounds, dependent: :destroy

  ROLES = [:user, :people_business_partner, :total_rewards, :admin].freeze
  enum role: ROLES

  def self.from_omniauth(email:, uid:)
    return nil unless email.match?(/@gitlab.com\z/)

    # link the user's previous account with Okta
    user = User.find_by(email: email)
    user.update(uid: uid) if user && user.uid.nil?

    user = User.find_or_create_by(uid: uid) do |user|
      user.uid = uid
      user.email = email
      user.password = Devise.friendly_token[0, 20]
    end

    if user.bamboo_id.nil?
      team_member = Bamboo.new.find_team_member(email: email)
      user.update!(bamboo_id: team_member['id']) if team_member
      raise "Bamboo ID not found for user #{user.id}" unless team_member
    end

    user
  end

  def has_reports?
    direct_reports.any?
  end

  def direct_reports
    bamboo_client.direct_reports(bamboo_id)
  end

  def all_reports
    if people_business_partner?
      department_reports = bamboo_client.reports_in_departments(departments, email)
      division_reports = bamboo_client.reports_in_divisions(divisions, email)
      own_reports = bamboo_client.all_reports(bamboo_id)

      return department_reports + division_reports + own_reports
    end

    bamboo_client.all_reports(bamboo_id)
  end

  def all_report_departments
    all_reports.pluck("department").uniq
  end

  def all_report_divisions
    all_reports.pluck("division").uniq
  end

  def report(team_member_id)
    all_reports.find do |team_member|
      team_member['id'] == team_member_id.to_s
    end
  end

  # Users are not allowed to change their email address.
  # Users should not get in here, because we don't have :registerable enabled
  def email=(address)
    raise 'Email can not be updated.' unless new_record?

    write_attribute(:email, address)
  end

  def active_for_authentication?
    super && !deactivated
  end

  def inactive_message
    deactivated? ? :has_been_deactivated : super
  end
end
