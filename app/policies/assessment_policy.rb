# frozen_string_literal: true

class AssessmentPolicy < ApplicationPolicy
  DIRECTORS = ['Director', 'Senior Director'].freeze
  ALLOW_ALL = ['Chief Executive Officer'].freeze

  def index?
    user&.has_reports? || user&.people_business_partner?
  end

  def table_view?
    user&.has_reports? || user&.people_business_partner?
  end

  def new?
    return false unless user

    return true if user.people_business_partner?

    # you can create a new assessment if
    # you are a manager of the person you are assessing
    # you are the skip level manager of the person you are assessing
    user.all_reports.any? { |dr| dr['id'] == record.team_member_bamboo_id.to_s }
  end

  def create?
    new?
  end

  def update_box_view?
    return false unless user
    return true if user.people_business_partner?

    user.all_reports.any? { |dr| dr['id'] == record.team_member_bamboo_id.to_s }
  end

  private

  def bamboo_client
    @bamboo_client ||= Bamboo.new
  end

  class Scope < Scope
    def resolve_for_round(assessment_round)
      return resolve_for_people_business_partner(assessment_round) if user.people_business_partner? && (user.departments.include?(assessment_round.department) || user.divisions.include?(assessment_round.division))
      return [[], Assessment.none] unless user.has_reports?

      reports = user.all_reports.select { |report| assessment_round.department == report['department'] } if assessment_round.department
      reports ||= user.all_reports.select { |report| assessment_round.division == report['division'] }
      reports.map { |report| report.except!('gender', 'customOtherGenderOptions') }
      assessments = scope.most_recent_in_round_for(reports, assessment_round)

      [reports, assessments]
    end

    def resolve_for_people_business_partner(assessment_round)
      reports = bamboo_client.reports_in_departments([assessment_round.department], user.email) if assessment_round.department
      reports ||= bamboo_client.reports_in_divisions([assessment_round.division], user.email)
      reports.map { |report| report.except!('gender', 'customOtherGenderOptions') }
      reports.map! do |report|
        report['4649.0']&.downcase!
        report['4643.0']&.sub!(' Potential', '')&.downcase!
        report
      end

      assessments = Assessment.most_recent_in_round(assessment_round)
      [reports, assessments]
    end

    def bamboo_client
      @bamboo_client ||= Bamboo.new
    end
  end
end
