# frozen_string_literal: true

class DashboardPolicy < ApplicationPolicy
  def index?
    user&.people_business_partner?
  end
end
