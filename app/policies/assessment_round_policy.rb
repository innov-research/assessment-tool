# frozen_string_literal: true

class AssessmentRoundPolicy < ApplicationPolicy
  def new?
    return false unless user&.people_business_partner?

    return user.departments.include?(record.department) if record.department.present?

    return user.divisions.include?(record.division) if record.division.present?

    true # the user is a PBP however there is no department or division set yet - only possible when new
  end

  def create?
    return false unless user&.people_business_partner?

    return user.departments.include?(record.department) if record.department.present?

    user.divisions.include?(record.division) if record.division.present?
  end

  def show?
    create?
  end

  def toggle_review_state?
    create?
  end

  def review?
    create?
  end

  def box_review?
    create?
  end

  def close?
    create?
  end

  def sync?
    create?
  end

  def download?
    create?
  end

  def update?
    create?
  end

  def edit?
    create?
  end

  def closed_view?
    create? # TODO: Rework for managers
  end
end
