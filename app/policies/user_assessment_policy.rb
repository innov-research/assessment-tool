# frozen_string_literal: true

class UserAssessmentPolicy < ApplicationPolicy
  def index?
    return false unless user

    # you are a manager of the person you are assessing
    # you are the skip level manager of the person you are assessing
    return true if user.all_reports.any? { |dr| dr['id'] == record['id'] }
  end

  class Scope < Scope
    attr_reader :assessment_for

    def initialize(user, scope, assessment_for)
      super(user, scope)
      @assessment_for = assessment_for
    end

    def resolve
      scope.where(team_member_bamboo_id: assessment_for['id']).order(created_at: :desc)
    end
  end
end
