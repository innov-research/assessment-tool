/* eslint no-console: 0 */
// Run this example by adding <%= javascript_pack_tag 'hello_vue' %> (and
// <%= stylesheet_pack_tag 'hello_vue' %> if you have styles in your component)
// to the head of your layout file,
// like app/views/layouts/application.html.erb.
// All it does is render <div>Hello Vue</div> at the bottom of the page.

import TurbolinksAdapter from 'vue-turbolinks';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'

import Vue from 'vue';
import AssessmentBox from '../assessment_box.vue';
import AssessmentSummary from '../assessment_summary.vue'

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(TurbolinksAdapter);

document.addEventListener('turbolinks:load', () => {
  const el = document.getElementById('assessment-box');

  if (el !== null) {
    const {reports} = el.dataset
    const {assessments} = el.dataset
    const {bambooId} = el.dataset
    const {assessmentRound} = el.dataset
    const {excluded} = el.dataset
    const {isPeopleBusinessPartner} = el.dataset

    // eslint-disable-next-line no-new
    new Vue({
      el,
      render: h => h(AssessmentBox, {
        props: {
          reports,
          assessments,
          bambooId,
          assessmentRound,
          excluded,
          isPeopleBusinessPartner: isPeopleBusinessPartner === 'true',
        }
      }),
    });
  }
});

document.addEventListener('turbolinks:load', () => {
  const el = document.getElementById('assessment-summary');

  if (el !== null) {
    const {assessments} = el.dataset
    const {reports} = el.dataset

    // eslint-disable-next-line no-new
    new Vue({
      el,
      render: h => h(AssessmentSummary, {
        props: {
          assessments,
          reports
        }
      }),
    });
  }
});