# frozen_string_literal: true

module AssessmentHelper
  def given_by(value, bamboo_id)
    user = User.find_by(id: value)
    user_on_bamboo = bamboo_client.find_team_member_by_id(user.bamboo_id.to_s)
    name = user_on_bamboo['fullName1']
    name += ' (self assessment)' if user.bamboo_id == bamboo_id
    name += ' (you)' if current_user == user
    name
  end

  def score(assessment)
    "#{assessment.score_potential} - #{assessment.score_performance}"
  end

  def last_assessment_at(bamboo_id, assessments)
    for_team_member = assessments.where(team_member_bamboo_id: bamboo_id).order(created_at: :desc).limit(1)
    return 'never assessed' if for_team_member.empty?

    "last assessed at #{for_team_member.first.created_at.to_formatted_s(:gitlab_iso)}"
  end

  def has_direct_reports?(bamboo_id)
    bamboo_client.direct_reports(bamboo_id).any?
  end

  def reports_for_title(reports_for)
    return "Your reports" if current_user.bamboo_id == reports_for['id']

    "Reports for #{reports_for['fullName1']}"
  end

  private

  def bamboo_client
    Bamboo.new
  end
end
