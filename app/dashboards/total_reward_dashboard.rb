# frozen_string_literal: true

require "administrate/custom_dashboard"

class TotalRewardDashboard < Administrate::CustomDashboard
  resource "Total Rewards"
end
