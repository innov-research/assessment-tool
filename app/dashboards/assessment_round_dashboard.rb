# frozen_string_literal: true

require 'administrate/base_dashboard'

class AssessmentRoundDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo,
    assessments: Field::HasMany,
    id: Field::Number,
    department: Field::String,
    start_date: Field::Date,
    due_date: Field::Date,
    state: Field::Select.with_options(searchable: false, collection: ->(field) { field.resource.class.public_send(field.attribute.to_s.pluralize).keys }), # rubocop:disable GitlabSecurity/PublicSend
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    division: Field::String,
    succession_planning: Field::Boolean,
    cut_off: Field::Date,
    promotion_planning: Field::Boolean,
    functional_lead_id: Field::Number
  }.freeze

  COLLECTION_ATTRIBUTES = %i[
    user
    assessments
    id
    department
    division
  ].freeze

  SHOW_PAGE_ATTRIBUTES = %i[
    user
    id
    department
    start_date
    due_date
    state
    created_at
    updated_at
    division
    succession_planning
    cut_off
    promotion_planning
    functional_lead_id
  ].freeze

  FORM_ATTRIBUTES = %i[
    department
    start_date
    due_date
    state
    division
    succession_planning
    cut_off
    promotion_planning
    functional_lead_id
  ].freeze

  COLLECTION_FILTERS = {}.freeze

  def display_resource(assessment_round)
    "Round for #{assessment_round.department || assessment_round.division}"
  end
end
