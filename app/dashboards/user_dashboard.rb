# frozen_string_literal: true

require "administrate/base_dashboard"

class UserDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    assessments: Field::HasMany,
    assessment_rounds: Field::HasMany,
    id: Field::Number,
    email: Field::String,
    encrypted_password: Field::String,
    reset_password_token: Field::String,
    reset_password_sent_at: Field::DateTime,
    remember_created_at: Field::DateTime,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    bamboo_id: Field::Number,
    failed_attempts: Field::Number,
    locked_at: Field::DateTime,
    unlock_token: Field::String,
    deactivated: Field::Boolean,
    role: Field::Select.with_options(searchable: false, collection: ->(field) { field.resource.class.public_send(field.attribute.to_s.pluralize).keys }), # rubocop:disable GitlabSecurity/PublicSend
    departments: Field::CollectionSelect.with_options(
      html_options: { multiple: true },
      collection: Bamboo.new.departments
    ),
    divisions: Field::CollectionSelect.with_options(
      html_options: { multiple: true },
      collection: Bamboo.new.divisions
    )
  }.freeze

  COLLECTION_ATTRIBUTES = %i[id email].freeze

  SHOW_PAGE_ATTRIBUTES = %i[
    id
    email
    created_at
    updated_at
    bamboo_id
    failed_attempts
    locked_at
    deactivated
    role
    departments
    divisions
  ].freeze

  FORM_ATTRIBUTES = %i[bamboo_id deactivated role departments divisions].freeze

  COLLECTION_FILTERS = {}.freeze

  def display_resource(user)
    user.email
  end
end
