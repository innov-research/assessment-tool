# frozen_string_literal: true

class HomeController < ApplicationController
  skip_before_action :authenticate_user!, only: [:about]
  before_action :set_open_assessment_rounds, only: [:index]
  # These aren't authorized tasks
  skip_after_action :verify_authorized

  # This is the default page after log in
  def index
    config = YAML.load_file('data/boxes_config.yml')
    @content = config.dig('introduction_text', 'content')
  end

  def about; end
end
