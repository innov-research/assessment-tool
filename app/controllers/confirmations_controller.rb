# frozen_string_literal: true

class ConfirmationsController < Devise::ConfirmationsController
  private

  def after_confirmation_path_for(resource_name, resource)
    # We switched to adding the BambooID on sign up. However there might
    # be some users in the mean time that had created their account but
    # not confirmed yet. So we keep this logic in the code for a bit.
    if resource.bamboo_id.nil?
      team_member = bamboo_client.find_team_member(email: resource.email)
      if team_member.nil?
        resource.destroy
        flash[:notice] = "We could not match this email account with a user on BambooHR. Please sign up again with the correct email address."
        redirect_to new_user_registration_path
      end

      resource.update!(bamboo_id: team_member['id'])
    end
    super
  end

  def bamboo_client
    @bamboo_client ||= Bamboo.new
  end
end
