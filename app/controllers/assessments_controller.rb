# frozen_string_literal: true

class AssessmentsController < ApplicationController
  before_action :set_open_assessment_rounds
  before_action :set_assessment_round, only: [:index, :box_review, :table_view]

  # GET /assessments
  # GET /assessments.json
  def index
    @reports, @assessments = AssessmentPolicy::Scope.new(current_user, Assessment).resolve_for_round(@assessment_round)
    @reports.each { |report| report['fullName1'] = Faker::Name.name } if params[:demo] || ENV['DEMO_DATA']
    authorize current_user, policy_class: AssessmentPolicy
  end

  def box_review
    @reports, @assessments = AssessmentPolicy::Scope.new(current_user, Assessment).resolve_for_people_business_partner(@assessment_round)
    @reports.each { |report| report['fullName1'] = Faker::Name.name } if params[:demo] || ENV['DEMO_DATA']
    @assessments = @assessments.reject { |assessment| params[:excluded_team_members]&.map(&:to_i)&.include? assessment.team_member_bamboo_id }
    @excluded_team_members = params.fetch(:excluded_team_members, [])

    authorize @assessment_round
  end

  def table_view
    @reports, @assessments = AssessmentPolicy::Scope.new(current_user, Assessment).resolve_for_round(@assessment_round)
    @reports.each { |report| report['fullName1'] = Faker::Name.name } if params[:demo] || ENV['DEMO_DATA']
    @reports.select! { |report| Date.parse(report['hireDate']).before?(@assessment_round.cut_off) } if @assessment_round.cut_off

    @confirmed_assessments = @assessments.where.not(confirmed_at: nil)
    authorize current_user, policy_class: AssessmentPolicy
  end

  def update_box_view
    assessment_round = @open_assessment_rounds.find(params['assessmentRound'])

    params[:reports].each do |report|
      next if report['emergencyContact'].nil? && (report['performance'].nil? || report['performance'] == 0) # rubocop:disable Style/NumericPredicate

      @assessment = current_user.assessments.new(
        score_performance: report['performance'],
        score_potential: report['potential'],
        team_member_bamboo_id: report['id'].to_i,
        assessment_round: assessment_round,
        emergency_contact: report['emergencyContact'],
        time_frame: report['readyWhen'],
        time_frame_promotion: report['readyForPromotion'],
        note: report['note']
      )

      authorize @assessment
      @assessment.save!
    end

    render json: { status: :created, message: "Assessments created" }
  end

  # GET /assessments/new
  def new
    @assessment_for = current_user.report(params[:team_member_bamboo_id])
    @assessment_round = @open_assessment_rounds.find(params[:assessment_round_id])

    @assessment = current_user.assessments.new(
      team_member_bamboo_id: params[:team_member_bamboo_id],
      assessment_round_id: @assessment_round.id
    )
    authorize @assessment
  end

  # POST /assessments
  # POST /assessments.json
  def create
    @assessment = current_user.assessments.new(assessment_params)

    authorize @assessment

    respond_to do |format|
      if @assessment.save
        format.html { redirect_to table_view_assessment_round_assessments_path(assessment_round: @assessment.assessment_round), notice: 'Assessment was successfully created.' }
        format.json { render :show, status: :created, location: @assessment }
      else
        format.html { redirect_to new_assessment_path }
        format.json { render json: @assessment.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_assessment
    @assessment = Assessment.find(params[:id])
  end

  def assessment_params
    params.require(:assessment).permit(:motivation, :team_member_bamboo_id, :score_performance, :score_potential, :assessment_round_id, :note)
  end

  def bamboo_client
    @bamboo_client ||= Bamboo.new
  end

  def set_assessment_round
    @assessment_round = @open_assessment_rounds.find(params[:assessment_round_id])
  rescue ActiveRecord::RecordNotFound
    flash[:notice] = 'Could not find an assessment round with that id'
    redirect_to root_path
  end
end
