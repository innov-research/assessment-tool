# frozen_string_literal: true

class UserAssessmentsController < ApplicationController
  before_action :set_assessment_for

  def index
    @assessments = UserAssessmentPolicy::Scope.new(current_user, Assessment, @assessment_for).resolve
    authorize @assessment_for, policy_class: UserAssessmentPolicy
  end

  private

  def set_assessment_for
    @assessment_for = current_user.report(params[:team_member_bamboo_id])
    return unless @assessment_for.nil?

    flash[:notice] = 'Could not find a user with that id'
    redirect_to root_path
  end
end
