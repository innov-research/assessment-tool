# frozen_string_literal: true

class DashboardController < ApplicationController
  def index
    @assessment_rounds = AssessmentRound.where(department: current_user.departments).or(AssessmentRound.where(division: current_user.divisions))
    authorize current_user, policy_class: DashboardPolicy
  end
end
