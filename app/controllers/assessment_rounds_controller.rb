# frozen_string_literal: true

class AssessmentRoundsController < ApplicationController
  before_action :set_assessment_round, only: [:show, :toggle_review_state, :review, :close, :sync, :download, :edit, :update, :closed_view]
  before_action :set_open_assessment_rounds, only: [:new]

  def closed_view
    @reports, @assessments = AssessmentPolicy::Scope.new(current_user, Assessment).resolve_for_round(@assessment_round)
    @reports.each { |report| report['fullName1'] = Faker::Name.name } if params[:demo] || ENV['DEMO_DATA']

    authorize @assessment_round
  end

  def new
    @assessment_round = current_user.assessment_rounds.new(department: params.dig(:department))
    @valid_departments = current_user.departments - @open_assessment_rounds.pluck(:department)
    authorize @assessment_round

    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @assessment_round = current_user.assessment_rounds.new(assessment_round_params)
    authorize @assessment_round

    if @assessment_round.save
      redirect_to assessment_round_path(@assessment_round), notice: 'Assessment Round was successfully created.'
    else
      render :new, error: 'Something went wrong'
    end
  end

  def show
    authorize @assessment_round

    @all_team_members_assessed = @assessment_round.all_team_members_assessed?
    @people_managers = @assessment_round.people_managers
  end

  def edit
    authorize @assessment_round
  end

  def update
    authorize @assessment_round

    if @assessment_round.update(assessment_round_update_params)
      redirect_to assessment_round_path(@assessment_round), notice: 'Assessment Round was successfully updated.'
    else
      render :new, error: 'Something went wrong'
    end
  end

  def toggle_review_state
    authorize @assessment_round

    @assessment_round.move_to_review!
    redirect_to assessment_round_path(@assessment_round)
  end

  def review
    authorize @assessment_round
    redirect_to assessment_round_path(@assessment_round), notice: 'The assessment round is not in review state' unless @assessment_round.in_review?

    @assessed_team_members = @assessment_round.assessed_team_members
  end

  def close
    authorize @assessment_round

    unless @assessment_round.in_review?
      flash[:notice] = 'The assessment should be reviewed first'
      redirect_to assessment_round_path(@assessment_round)
      return
    end

    Assessment.most_recent_in_round(@assessment_round).update(confirmed_at: DateTime.current)

    @assessment_round.close!
    redirect_to assessment_round_path(@assessment_round)
  end

  def sync
    authorize @assessment_round

    unless @assessment_round.closed?
      flash[:notice] = 'This assessment round is not closed. Please close the assessment round first.'
      redirect_to assessment_round_path(@assessment_round)
      return
    end

    flash[:notice] = if @assessment_round.sync_to_bamboo!
                       'Successfully synced to BambooHR.'
                     else
                       flash[:notice] = 'There was a problem syncing the data to BambooHR. Try again or contact People Ops Engineering.'
                     end

    redirect_to assessment_round_path(@assessment_round)
  end

  def download
    authorize @assessment_round
    file_data = @assessment_round.generate_csv

    respond_to do |format|
      format.csv { send_data file_data, filename: "assessments-#{Date.today}.csv" }
    end
  end

  private

  def assessment_round_params
    params.require(:assessment_round).permit(:department, :due_date, :start_date, :promotion_planning, :succession_planning, :cut_off, :load_previous_assessments)
  end

  def assessment_round_update_params
    params.require(:assessment_round).permit(:due_date, :start_date, :promotion_planning, :succession_planning, :cut_off, :state)
  end

  def set_assessment_round
    @assessment_round = AssessmentRound.where(department: current_user.departments).or(AssessmentRound.where(division: current_user.divisions)).find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:notice] = 'Can not find this Assessment round'
    redirect_to root_path
  end
end
