# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  include Pundit
  after_action :verify_authorized, unless: :devise_controller?
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  protect_from_forgery

  before_action :set_raven_context

  private

  def user_not_authorized
    flash[:alert] = "You are not authorized to perform this action."
    redirect_to(request&.referer || root_path)
  end

  def set_raven_context
    Raven.user_context(id: session[:current_user_id])
    Raven.extra_context(params: params.to_unsafe_h, url: request.url)
  end

  def after_sign_in_path_for(resource)
    # Warn existing users when they sign in if they are using a password from the PwnedPasswords dataset.
    set_flash_message! :alert, :warn_pwned if resource.respond_to?(:pwned?) && resource.pwned?
    super
  end

  def set_open_assessment_rounds
    @open_assessment_rounds = if current_user.people_business_partner?
                                AssessmentRound.non_closed.where(department: current_user.departments).or(AssessmentRound.non_closed.where(division: current_user.divisions))
                                  .or(AssessmentRound.open.where(department: current_user.all_report_departments))
                              else
                                AssessmentRound.open.where(department: current_user.all_report_departments).or(AssessmentRound.open.where(division: current_user.all_report_divisions))
                              end
  end
end
