# frozen_string_literal: true

module Admin
  class UsersController < Admin::ApplicationController
    def impersonate
      raise 'You are in production. Do not run this.' if Rails.env.production?

      user = User.find(params[:id])
      bypass_sign_in(user)
      redirect_to root_url
    end
  end
end
