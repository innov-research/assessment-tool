# frozen_string_literal: true

class Admin::TotalRewardsController < Admin::ApplicationController
  def index
    @assessment_rounds = AssessmentRound.all.order(:created_at)
  end

  def download
    @assessment_rounds = AssessmentRound.where(id: params[:rounds])

    file_data = CSV.generate(headers: true) do |csv|
      csv << %w[id name performance]
      @assessment_rounds.each do |round|
        round.total_rewards_data.each do |user_data|
          csv << user_data
        end
      end
    end

    respond_to do |format|
      format.csv { send_data file_data, filename: "assessments-#{Date.today}.csv" }
    end
  end

  private

  def authenticate_admin
    return if current_user.total_rewards? || current_user.admin?

    flash[:alert] = 'You are not an admin or total rewards team member'
    redirect_to root_path
  end
end
