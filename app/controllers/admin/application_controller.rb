# frozen_string_literal: true

module Admin
  class ApplicationController < Administrate::ApplicationController
    before_action :authenticate_user!
    before_action :authenticate_admin

    private

    def authenticate_admin
      return if current_user.admin?

      flash[:alert] = 'You are not an admin'
      redirect_to root_path
    end

    def read_param_value(data)
      data.delete("") if data.is_a?(Array)

      super(data)
    end
  end
end
