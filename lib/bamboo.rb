# frozen_string_literal: true

require 'bamboozled'

class Bamboo
  def initialize
    @client = Bamboozled.client(subdomain: 'gitlab', api_key: ENV['BAMBOO_API_KEY'])
  end

  def find_team_member(email:)
    active_team_members.find { |team_member| team_member['workEmail'] == email }
  end

  def find_team_member_by_id(id)
    active_team_members.find { |team_member| team_member['id'] == id.to_s }
  end

  def find_team_members_by_department(department)
    active_team_members.select { |team_member| team_member['department'] == department }
  end

  def find_team_members_by_division(division)
    active_team_members.select { |team_member| team_member['division'] == division }
  end

  def active_team_members
    today = Date.current
    @active_team_members ||= team_members.select do |team_member|
      team_member['workEmail'].present? && Date.parse(team_member['hireDate']) <= today
    end
  end

  def all_reports(id)
    direct_reports_to_check = direct_reports(id)
    reports_to_return = []
    while direct_reports_to_check.present?
      current_node = direct_reports_to_check.shift
      reports_to_return << current_node
      direct_reports_to_check.concat(direct_reports(current_node['id']))
    end
    reports_to_return
  end

  def direct_reports(id)
    active_team_members.select { |team_member| team_member['supervisorEId'] == id.to_s }
  end

  def reports_in_departments(departments, email)
    active_team_members.select do |team_member|
      # Filter out the PBP itself in case their department is also the department they manage
      departments.include?(team_member['department']) && team_member['workEmail'] != email
    end
  end

  def reports_in_divisions(divisions, email)
    active_team_members.select do |team_member|
      # Filter out the PBP itself in case their department is also the department they manage
      divisions.include?(team_member['division']) && team_member['workEmail'] != email
    end
  end

  def add_rating_details(team_member_id, performance, potential)
    return true if ENV['DRY_RUN']

    data = {
      customEffectiveDate4: Date.today.to_s,
      customPerformanceFactor: performance,
      customPotentialFactor: potential
    }
    @client.employee.add_table_row(team_member_id, 'customPerformanceandPotentialTable', data)
  end

  def departments
    meta_fields.detect { |res| res['name'] == 'Department' }.dig('options').each_with_object([]) { |option, array| array << option['name'] if option['archived'] == 'no' } || []
  end

  def divisions
    meta_fields.detect { |res| res['name'] == 'Division' }.dig('options').each_with_object([]) { |option, array| array << option['name'] if option['archived'] == 'no' } || []
  end

  private

  def team_members
    Rails.cache.fetch("team_members", expires_in: 12.hours) do
      @team_members ||= @client.report.find(ENV['BAMBOO_REPORT_ID'], 'JSON', true).reject { |employee| employee['lastName'] == 'Test-Gitlab' }
    end
  end

  def meta_fields
    @meta_fields ||= @client.meta.lists
  end
end
