introduction_text:
  content: |
    Hi there, <br /><br />
    Welcome to GitLab's Performance/Potential Assessment Tool! <br /><br />
    Before we kick off we wanted to share two essential links to the handbook where you can read up on more information with regards to the Performance/Potential Assessment process and the use of this tool,
    <ul>
      <li> <a href='https://about.gitlab.com/handbook/people-group/performance-assessments-and-succession-planning'>Performance/Potential Assessment page</a></li>
      <li> <a href='https://about.gitlab.com/handbook/people-group/engineering/assessment-tool/'>Handbook for the use of the tool</a></li>
    </ul>
    If you are an individual contributor visiting this page, you don't need to do anything during the potential/performance assessment. Your manager will reach out to you once the assessments have been completed to have a discussion on your performance.
    <br /><br />
    If you are a people manager visiting this page and your People Business Partner has started the assessment round for your group, you can start the assessment by clicking "Assess your team members within [TEAM]" and kick off the assessment. For more information on the next steps please visit <a href='https://about.gitlab.com/handbook/people-group/engineering/assessment-tool/#people-managers'>this page</a> in the handbook.
    <br /><br />
    In case you have any feedback on the tool, please reach out to your People Business Partner.
    <br /><br />
    Have a great assessment round!
boxes:
  one:
    display: 1
    title: Star
    content: |
      Team member is developing faster than the demands of their current position and/or division. Team member has been given additional assignments and has demonstrated high-level commitment/achieved significant results. Team member is ready to broaden their skill set and take on significantly greater scope and responsibility.

  two:
    display: 2
    title: High Performer
    content: |
      Team member performs well in their current job, makes valuable contributions
      and consistently demonstrates competencies required. May be ready to take on greater
      scope and responsibility in the next 12 months
  three:
    display: 3
    title: High Potential
    content: |
      Team member is contributing as expected and is meeting performance expectations. Team member
      may be ready to take on greater technical and/or leadership responsibility in
      the next 6-12 months.
  four:
    display: 4
    title: Core Performer
    content: |
      Team member is currently meeting expectations but may not be willing or
      able to advance; may not be ready to absorb additional scope or complexity in
      the next 12 months.
  five:
    display: 5
    title: Solid Performer
    content: |
      Team member is performing well in their current job but needs to continue
      development in current role, or has not exhibited greater technical and/or
      leadership potential. Team member has not demonstrated willingness to take
      on significantly greater scope and responsibility in the next 12-24 months.
  six:
    display: 6
    title: Potential Gem
    content: |
      Team member is not meeting the requirements in their current role. It is
      possible that team member could be more successful in the current role with more
      direction or in another role or department that more appropriately suits their
      skill set.
  seven:
    display: 7
    title: Average Performer
    content: |
      Team member is currently meeting expectations of their role.. Team member
      is not prepared to absorb additional scope or complexity in the next 12-24 months.
  eight:
    display: 8
    title: Inconsistent Performer
    content: |
      Team member has not been in the position long enough to adequately demonstrate
      their technical abilities, or may have lost pace with changes in the organization.
  nine:
    display: 9
    title: Lower Performer
    content: |
      Team member is not meeting performance expectations and there is still
      more to learn in the current position. There are questions about their ability
      to succeed in the current role long-term.
