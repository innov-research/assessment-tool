# Assessment tool

This project is created by People Group Engineering and is used by the whole GitLab team to help with running 9 box assessments. Feedback and bug can be reported in this project by creating a new issue.

### Mirrored Project

This project is mirrored to a private project on [ops.gitlab.net](ops.gitlab.net) and is deployed from there to GCP using Auto Devops. The reason for the private mirror is the following:

> Anyone with Maintainer or above access to `gitlab-com` group can find the tokens used on CI and use it.

### Development

If you want to run the project/specs locally, you need to do the following:

1. Clone this project
1. You need a BambooHR API key set up as the ENV variable `BAMBOO_API_KEY`

At this point you should be able to run

```
rails db:setup
rails s
```
