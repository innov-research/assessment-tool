Rails.application.routes.draw do
  namespace :admin do
    resources :total_rewards, only: :index do
      collection do
        get 'download'
      end
    end

    resources :users do
      member do
        get 'impersonate'
      end
    end
    resources :assessment_rounds

    root to: "users#index"
  end

  resources :user_assessments, only: [:index]
  resources :assessment_rounds, only: [:new, :create, :show, :edit, :update] do
    resources :assessments, only: [:index, :new, :create] do
      collection do
        get 'table_view'
        get 'box_review'
        post 'update_box_view'
      end
    end

    member do
      get 'closed_view'
      get 'review'
      get 'download'
      post 'toggle_review_state'
      post 'close'
      post 'sync'
    end
  end

  devise_for :users, controllers: {
    confirmations: 'confirmations',
    omniauth_callbacks: 'users/omniauth_callbacks'
  }

  root 'home#index'
  get '/about', to: 'home#about'
  get '/dashboard', to: 'dashboard#index'
end
