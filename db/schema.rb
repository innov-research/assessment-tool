# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_04_02_133913) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assessment_rounds", force: :cascade do |t|
    t.string "department"
    t.date "start_date"
    t.date "due_date"
    t.integer "state", default: 0
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "division"
    t.boolean "succession_planning", default: false
    t.date "cut_off"
    t.boolean "promotion_planning", default: false
    t.integer "functional_lead_id"
    t.boolean "load_previous_assessments", default: false
    t.index ["user_id"], name: "index_assessment_rounds_on_user_id"
  end

  create_table "assessments", force: :cascade do |t|
    t.text "motivation"
    t.integer "team_member_bamboo_id"
    t.integer "given_by"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "confirmed_at"
    t.integer "score_potential"
    t.integer "score_performance"
    t.bigint "assessment_round_id", null: false
    t.datetime "synced_at"
    t.string "time_frame"
    t.string "time_frame_promotion"
    t.text "note"
    t.string "emergency_contact"
    t.index ["assessment_round_id"], name: "index_assessments_on_assessment_round_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "bamboo_id"
    t.integer "failed_attempts", default: 0, null: false
    t.datetime "locked_at"
    t.string "unlock_token"
    t.boolean "deactivated", default: false
    t.integer "role"
    t.string "departments", default: [], array: true
    t.string "divisions", default: [], array: true
    t.string "uid"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "assessment_rounds", "users"
  add_foreign_key "assessments", "assessment_rounds"
end
