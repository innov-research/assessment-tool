# frozen_string_literal: true

class AddAssessmentRoundToAssessments < ActiveRecord::Migration[6.0]
  def change
    add_reference :assessments, :assessment_round, null: true, foreign_key: true
  end
end
