# frozen_string_literal: true

class AddPromotionPlanningToAssessmentRounds < ActiveRecord::Migration[6.0]
  def change
    add_column :assessment_rounds, :promotion_planning, :boolean, default: false
  end
end
