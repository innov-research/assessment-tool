# frozen_string_literal: true

class ReworkAssessmentsTable < ActiveRecord::Migration[6.1]
  def change
    remove_column :assessments, :emergency_contact, :boolean, default: false
    rename_column :assessments, :emegercy_contactx, :emergency_contact
  end
end
