# frozen_string_literal: true

class AddTwoScoreFieldsToAssessments < ActiveRecord::Migration[6.0]
  def change
    change_table :assessments, bulk: true do |t|
      t.integer :score_potential
      t.integer :score_performance
    end
    remove_column :assessments, :score, :integer
  end
end
