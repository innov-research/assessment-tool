# frozen_string_literal: true

class AddNotesToAssessments < ActiveRecord::Migration[6.0]
  def change
    add_column :assessments, :note, :text
  end
end
