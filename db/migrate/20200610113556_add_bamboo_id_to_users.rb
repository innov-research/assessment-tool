# frozen_string_literal: true

class AddBambooIdToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :bamboo_id, :integer
  end
end
