# frozen_string_literal: true

class CreateAssessments < ActiveRecord::Migration[6.0]
  def change
    create_table :assessments do |t|
      t.text :motivation
      t.integer :team_member_bamboo_id
      t.integer :given_by

      t.timestamps
    end
  end
end
