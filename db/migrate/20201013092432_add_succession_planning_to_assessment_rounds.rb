# frozen_string_literal: true

class AddSuccessionPlanningToAssessmentRounds < ActiveRecord::Migration[6.0]
  def change
    add_column :assessment_rounds, :succession_planning, :boolean, default: false
  end
end
