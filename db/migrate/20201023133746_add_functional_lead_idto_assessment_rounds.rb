# frozen_string_literal: true

class AddFunctionalLeadIdtoAssessmentRounds < ActiveRecord::Migration[6.0]
  def change
    add_column :assessment_rounds, :functional_lead_id, :integer
  end
end
