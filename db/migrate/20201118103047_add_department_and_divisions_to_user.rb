# frozen_string_literal: true

class AddDepartmentAndDivisionsToUser < ActiveRecord::Migration[6.0]
  def change
    change_table :users, bulk: true do |t|
      t.string :departments, array: true, default: []
      t.string :divisions, array: true, default: []
    end
  end
end
