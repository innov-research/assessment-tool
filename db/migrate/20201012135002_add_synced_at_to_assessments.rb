# frozen_string_literal: true

class AddSyncedAtToAssessments < ActiveRecord::Migration[6.0]
  def change
    add_column :assessments, :synced_at, :datetime
  end
end
