# frozen_string_literal: true

class CreateAssessmentRounds < ActiveRecord::Migration[6.0]
  def change
    create_table :assessment_rounds do |t|
      t.string :department
      t.date :start_date
      t.date :due_date
      t.integer :state
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
