# frozen_string_literal: true

class AddScoreToAssessments < ActiveRecord::Migration[6.0]
  def change
    add_column :assessments, :score, :integer
  end
end
