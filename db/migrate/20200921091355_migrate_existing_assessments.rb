# frozen_string_literal: true

class MigrateExistingAssessments < ActiveRecord::Migration[6.0]
  def change
    if Assessment.any?
      user = User.find_by(email: 'jarmendariz@gitlab.com')
      assessment_round = AssessmentRound.create(
        user: user,
        department: 'People Success'
      )

      Assessment.update_all(assessment_round_id: assessment_round.id) # rubocop:disable Rails/SkipsModelValidations
    end
    change_column :assessments, :assessment_round_id, :bigint, null: false
  end
end
