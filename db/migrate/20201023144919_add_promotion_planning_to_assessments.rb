# frozen_string_literal: true

class AddPromotionPlanningToAssessments < ActiveRecord::Migration[6.0]
  def change
    add_column :assessments, :time_frame_promotion, :string
  end
end
