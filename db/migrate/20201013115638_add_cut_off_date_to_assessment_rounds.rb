# frozen_string_literal: true

class AddCutOffDateToAssessmentRounds < ActiveRecord::Migration[6.0]
  def change
    add_column :assessment_rounds, :cut_off, :date
  end
end
