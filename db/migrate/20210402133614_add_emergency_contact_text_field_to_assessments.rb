# frozen_string_literal: true

class AddEmergencyContactTextFieldToAssessments < ActiveRecord::Migration[6.1]
  def change
    add_column :assessments, :emegercy_contactx, :string
  end

  def up
    Assessment.where(emergency_contact: true).update(emegercy_contactx: 'Yes')
  end
end
