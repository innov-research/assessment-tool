# frozen_string_literal: true

class AddDivisionToAssessmentRound < ActiveRecord::Migration[6.0]
  def change
    add_column :assessment_rounds, :division, :string
  end
end
