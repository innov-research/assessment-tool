# frozen_string_literal: true

class AddDefaultValueForStateAssessmentRounds < ActiveRecord::Migration[6.0]
  def change
    change_column_default :assessment_rounds, :state, from: nil, to: 0
  end
end
