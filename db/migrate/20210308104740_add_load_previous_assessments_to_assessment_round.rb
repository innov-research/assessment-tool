# frozen_string_literal: true

class AddLoadPreviousAssessmentsToAssessmentRound < ActiveRecord::Migration[6.0]
  def change
    add_column :assessment_rounds, :load_previous_assessments, :boolean, default: false
  end
end
