# frozen_string_literal: true

class AddFunctionalLeadToAllAssessmentRounds < ActiveRecord::Migration[6.0]
  def change
    AssessmentRound.find_each do |round|
      round.set_functional_lead
      round.save
    end
  end
end
