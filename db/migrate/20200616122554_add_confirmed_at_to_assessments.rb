# frozen_string_literal: true

class AddConfirmedAtToAssessments < ActiveRecord::Migration[6.0]
  def change
    add_column :assessments, :confirmed_at, :datetime
  end
end
