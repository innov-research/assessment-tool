# frozen_string_literal: true

class AddRoleToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :role, :integer # rubocop:disable Rails/BulkChangeTable
    remove_column :users, :admin, :boolean, default: false
  end
end
