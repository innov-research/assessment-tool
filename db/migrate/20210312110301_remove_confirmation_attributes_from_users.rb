# frozen_string_literal: true

class RemoveConfirmationAttributesFromUsers < ActiveRecord::Migration[6.0]
  def up
    change_table :users, bulk: true do |t|
      t.remove :confirmation_sent_at, :confirmation_token, :confirmed_at
    end
  end

  def down
    change_table :users, bulk: true do |t|
      t.datetime :confirmation_sent_at
      t.string :confirmation_token
      t.datetime :confirmed_at
    end
  end
end
