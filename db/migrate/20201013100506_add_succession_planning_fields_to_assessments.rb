# frozen_string_literal: true

class AddSuccessionPlanningFieldsToAssessments < ActiveRecord::Migration[6.0]
  def change
    change_table :assessments, bulk: true do |t|
      t.string :time_frame
      t.boolean :emergency_contact, default: false
    end
  end
end
