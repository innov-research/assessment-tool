# frozen_string_literal: true
raise 'You are in production. Do not run this.' if Rails.env.production?

AssessmentRound.destroy_all
User.destroy_all

# ------------------- People Success In Review round ---------
# Create some users within People Success
julie = User.create!(
  email: 'jarmendariz@gitlab.com',
  password: 'Mz23wuVNXwcDGTk',
  bamboo_id: 40809,
  role: :people_business_partner,
  departments: ['Accounting', 'Business Operations', 'Finance', 'Legal', 'People Success', 'Recruiting', 'Brand & Digital Design', 'Corporate Marketing', 'Digital Marketing', 'Field Marketing', 'Marketing Ops', 'Product Marketing', 'CEO'],
  divisions: %w[Marketing]
)
carol = User.create!(
  email: 'cteskey@gitlab.com',
  password: 'Mz23wuVNXwcDGTk',
  bamboo_id: 41013,
  role: :people_business_partner,
  departments: ['Legal', 'People Success', 'Recruiting']
)

# Create an Assessment round
round = julie.assessment_rounds.create!(
  department: 'People Success',
  start_date: Date.today,
  due_date: Date.today + 7.days,
  succession_planning: true,
  functional_lead_id: 41013
)

# Add assessments to the round
%w[40562 40623 40708 40738 40740 40809 40811 41125 41183 41201 41266 41415 41521 41558 41595 41766 41788 41812 41907 41913 41924 41942 41964 42002 41054 41721].each do |id|
  carol.assessments.create!(
    assessment_round: round,
    score_performance: rand(0..2),
    score_potential: rand(0..2),
    team_member_bamboo_id: id.to_i
  )
end

# Move to in_review
round.move_to_review!
# ------------------- End People Success In Review round ---------

# ------------------- Recruiting Open round ----------------------
# Create an Assessment round
recruiting_round = julie.assessment_rounds.create!(
  department: 'Recruiting',
  start_date: Date.today,
  due_date: Date.today + 7.days,
  functional_lead_id: 41557
)

# Add some assessments
%w[41023 40945 41598].each do |id|
  julie.assessments.create!(
    assessment_round: recruiting_round,
    score_performance: rand(0..2),
    score_potential: rand(0..2),
    team_member_bamboo_id: id.to_i
  )
end
# ------------------- End Recruiting Open round --------------------

# ------------------- Finance Closed round ----------------------

finance_round = julie.assessment_rounds.create!(
  department: 'Finance',
  start_date: Date.today,
  due_date: Date.today + 7.days,
  functional_lead_id: 15
)
# Add assessments to the round
%w[40796 40880 41137 41236 41573 41594 41727 41836 41923 42048 42096].each do |id|
  julie.assessments.create!(
    assessment_round: finance_round,
    score_performance: rand(0..2),
    score_potential: rand(0..2),
    team_member_bamboo_id: id.to_i
  )
end
finance_round.close!
# ------------------- End Finance Closed round ----------------------

# ------------------- Legal Open All done round ----------------------
robin = User.create!(
  email: 'rschulman@gitlab.com',
  password: 'Mz23wuVNXwcDGTk',
  bamboo_id: 41644
)

legal_round = julie.assessment_rounds.create!(
  department: 'Legal',
  start_date: Date.today,
  due_date: Date.today + 7.days,
  functional_lead_id: 41644
)

%w[40925 41386 41449 41831 41839 41891 42011 42079 42097].each do |id|
  robin.assessments.create!(
    assessment_round: legal_round,
    score_performance: rand(0..2),
    score_potential: rand(0..2),
    team_member_bamboo_id: id.to_i
  )
end
# ------------------- End Legal Open All done round ----------------------

carolyn = User.create!(
  email: 'cbednarz@gitlab.com',
  password: 'Mz23wuVNXwcDGTk',
  bamboo_id: 41812,
  role: :people_business_partner,
  divisions: ['Sales'],
  departments: ['Alliances', 'Channel', 'Commercial Sales', 'Consulting Delivery', 'Customer Success', 'Education Delivery', 'Enterprise Sales', 'Field Operations', 'Practice Management', 'CEO']
)

carolyn.assessment_rounds.create!(
  division: 'Sales',
  start_date: Date.today,
  due_date: Date.today + 7.days,
  functional_lead_id: 123
)

jessica = User.create!(
  email: 'jmitchell@gitlab.com',
  password: 'Mz23wuVNXwcDGTk',
  bamboo_id: 40740,
  role: :people_business_partner,
  divisions: %w[Product Marketing],
  departments: ['Meltano', ' Product Management', 'Product Strategy', 'Brand & Digital Design', 'Community Relations', 'Corporate Marketing', 'Digital Marketing', 'Field Marketing', 'Marketing Ops', 'Product Marketing']
)

jessica.assessment_rounds.create!(
  division: 'Product',
  start_date: Date.today,
  due_date: Date.today + 7.days,
  functional_lead_id: 123
)

lorna = User.create!(
  email: 'lwebster@gitlab.com',
  password: 'Mz23wuVNXwcDGTk',
  bamboo_id: 40740,
  role: :people_business_partner,
  divisions: ['Sales'],
  departments: ['Alliances', 'Channel', 'Commercial Sales', 'Consulting Delivery', 'Customer Success', 'Education Delivery', 'Enterprise Sales', 'Field Operations', 'Practice Management']
)

lorna.assessment_rounds.create!(
  department: 'Customer Success',
  start_date: Date.today,
  due_date: Date.today + 7.days,
  functional_lead_id: 123
)

User.create!(
  email: 'lvandensteen@gitlab.com',
  password: 'Mz23wuVNXwcDGTk',
  role: :admin
)

User.create!(
  email: 'brittany@gitlab.com',
  password: 'Mz23wuVNXwcDGTk',
  role: :total_rewards
)
