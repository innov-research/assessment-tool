
## Threat Model
We're following the [Evidence Driven PASTA Threat Modeling approach](https://about.gitlab.com/handbook/security/threat_modeling/#evidence-driven-threat-model)
as [outlined in the handbook](https://about.gitlab.com/handbook/security/threat_modeling/)

### Application Decomposition
* The assessment tool allows GitLab managers to assess their (indirect) reports in a 9 box assessment.
* This whole flow is managed by the GitLab People Business Partners.
* The tool has read/write access to data on BambooHR and has the ability to sync the assessment results to the team member's BambooHR profile.
* Entry point is the rails application at `https://assessment.gitlab.net/`. See `routes.rb` for all the possible routes.
* Different trust levels/domains:
  * Three user trust levels: People Business Partners, Managers and Individual Contributors
    * People Business Partners can start and end assessment rounds for the departments and divisions they have access to. They can also assess those team members. They can sync the data to BambooHR
    * Managers can assess team members when an assessment round has been opened by the People Business Partners
    * ICs can sign up, however there is nothing for them in the tool

##### Threat overview
Threats beyond the usual web application threats (e.g. OWASP Top Ten):

|Threat| Tests |Comment / Notes|
|-------|-------|-------|
|Unauthenticated Users getting access to data| TBD | TBD |
|Managers have access to more than their reporting structure| TBD | TBD |
|Team members have access to data| TBD | TBD |
|Confidential BambooHR is leaked| TBD | TBD |

##### Unauthenticated Users getting access to data

This might be caused by lack of authentication and authorization or authentication and authorization bypasses.
We prevent this by having the highest authentication/authorization check on the `ApplicationController`.
Every other controller is in charge of determining if we need to go down in authentication instead of up.

Todo:
- [ ] Routes need to be specced on authentication level. So that changes would be noticed.
- [ ] Remove sign up for non-managers. Since there is nothing for them to do in the tool, we should not allow them to sign up

**Tests:** Authentication / Authorization e.g. login/logout, password reset, login requirements.

##### Managers have access to more than their reporting structure

Flaws in the code could result in managers being able to see more than only their (indirect) reports.

Todo:
- [ ] Check if all controller actions have specific authorization specs to test that there is only access to the reporting structure.

**Tests:** Authorization

##### Individual Contributers have access to data

Individual contributors should not be able to sign up.

##### Confidential BambooHR is leaked

The tool has access to some confidential data linked to team members (age, gender, location). Flaws in the code
could expose this.


#### DFD:

TODO
