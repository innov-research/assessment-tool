# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.2'

# gems shipped with rails
gem 'rails', '6.1.3.1'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 4.1'
gem 'sass-rails', '>= 6'
gem 'webpacker', '~> 4.0'
gem 'turbolinks', '~> 5'
gem 'bootsnap', '>= 1.4.2', require: false

# added by us
gem "administrate"
gem 'administrate-field-collection_select', git: 'https://gitlab.com/gitlab-com/people-group/peopleops-eng/administrate-field-collection_select.git' # rubocop:disable Cop/GemFetcher
gem 'bamboozled', git: 'https://gitlab.com/gitlab-com/people-group/peopleops-eng/bamboozled.git', branch: 'v0.2.0-gitlab' # rubocop:disable Cop/GemFetcher
gem 'devise'
gem 'devise-pwned_password', '~> 0.1.9'
gem 'devise-token_authenticatable'
gem 'faker'
gem 'omniauth'
gem 'omniauth-oktaoauth'
gem 'omniauth-rails_csrf_protection'
gem 'pundit'
gem 'sentry-raven'
gem 'simple_form'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'capybara'
  gem 'dotenv-rails'
  gem 'factory_bot_rails'
  gem "letter_opener"
  gem 'rails-controller-testing'
  gem 'rspec-rails'
  gem 'selenium-webdriver', '~> 3.142'
end

group :development do
  gem 'annotate'
  gem "better_errors"
  gem "binding_of_caller"
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :rubocop do
  gem 'rubocop'
  gem 'rubocop-rspec'
  gem 'gitlab-styles', '~> 3.1', require: false
end

group :test do
  gem 'capybara-screenshot'
  gem 'database_cleaner-active_record'
  gem 'pundit-matchers', '~> 1.6.0'
  gem 'shoulda-matchers'
  gem 'simplecov', require: false
  gem 'webmock'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
