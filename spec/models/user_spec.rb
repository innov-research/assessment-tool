# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  deactivated            :boolean          default(FALSE)
#  departments            :string           default([]), is an Array
#  divisions              :string           default([]), is an Array
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  failed_attempts        :integer          default(0), not null
#  locked_at              :datetime
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  role                   :integer
#  uid                    :string
#  unlock_token           :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  bamboo_id              :integer
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#
require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { build(:user) }

  it "has a valid factory" do
    expect(user).to be_valid
  end

  describe 'email validation' do
    it 'fails when it has an illegal email' do
      user.email = '<me@evil.com>whatever@gitlab.com'
      expect(user).not_to be_valid
    end

    it 'fails when it does not end with gitlab.com' do
      user.email = 'michael@glab.com'
      expect(user).not_to be_valid
    end
  end

  describe '#people_business_partner?' do
    context 'when they are a pbp' do
      before do
        user.email = 'jarmendariz@gitlab.com'
        user.role = :people_business_partner
      end

      it 'returns true' do
        expect(user).to be_people_business_partner
      end
    end

    context 'when they are not a pbp' do
      it 'returns false' do
        expect(user).not_to be_people_business_partner
      end
    end
  end

  describe '#departments' do
    it 'returns an empty array' do
      expect(user.departments).to eq []
    end

    context 'when it is a pbp' do
      before do
        user.email = 'jarmendariz@gitlab.com'
        user.role = :people_business_partner
        user.departments = ['Accounting', 'People Success']
      end

      it 'returns the departments' do
        expect(user.departments).to eq ['Accounting', 'People Success']
      end
    end
  end

  describe '#divisions' do
    it 'returns an empty array' do
      expect(user.divisions).to eq []
    end

    context 'when it is a pbp' do
      before do
        user.email = 'jarmendariz@gitlab.com'
        user.role = :people_business_partner
        user.divisions = ['Marketing']
      end

      it 'returns the divisions' do
        expect(user.divisions).to eq ['Marketing']
      end
    end
  end

  describe '#email=' do
    let(:user) { create(:user) }

    it 'does not allow to edit the email' do
      expect { user.email = 'new-email@gitlab.com' }.to raise_error('Email can not be updated.')
    end
  end
end
