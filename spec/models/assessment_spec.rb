# frozen_string_literal: true

# == Schema Information
#
# Table name: assessments
#
#  id                    :bigint           not null, primary key
#  confirmed_at          :datetime
#  emergency_contact     :string
#  given_by              :integer
#  motivation            :text
#  note                  :text
#  score_performance     :integer
#  score_potential       :integer
#  synced_at             :datetime
#  time_frame            :string
#  time_frame_promotion  :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  assessment_round_id   :bigint           not null
#  team_member_bamboo_id :integer
#
# Indexes
#
#  index_assessments_on_assessment_round_id  (assessment_round_id)
#
# Foreign Keys
#
#  fk_rails_...  (assessment_round_id => assessment_rounds.id)
#
require 'rails_helper'

RSpec.describe Assessment, type: :model do
  it "has a valid factory" do
    expect(create(:assessment)).to be_valid
  end

  it { is_expected.to validate_presence_of(:given_by) }
  it { is_expected.to validate_presence_of(:team_member_bamboo_id) }
  it { is_expected.to belong_to(:assessment_round) }
end
