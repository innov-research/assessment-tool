# frozen_string_literal: true

# == Schema Information
#
# Table name: assessment_rounds
#
#  id                        :bigint           not null, primary key
#  cut_off                   :date
#  department                :string
#  division                  :string
#  due_date                  :date
#  load_previous_assessments :boolean          default(FALSE)
#  promotion_planning        :boolean          default(FALSE)
#  start_date                :date
#  state                     :integer          default("open")
#  succession_planning       :boolean          default(FALSE)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  functional_lead_id        :integer
#  user_id                   :bigint           not null
#
# Indexes
#
#  index_assessment_rounds_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
require 'rails_helper'

RSpec.describe AssessmentRound, type: :model do
  let(:assessment_round) { build(:assessment_round) }

  it "has a valid factory" do
    expect(create(:assessment_round)).to be_valid
  end

  describe 'validations' do
    let(:round) { build(:assessment_round) }

    it 'is not valid without a user' do
      round.user = nil
      expect(round).not_to be_valid
    end

    it 'is not valid without a department' do
      round.department = nil
      expect(round).not_to be_valid
    end
  end

  describe '#only_one_open_per_department' do
    before do
      create(:assessment_round)
    end

    it 'is not valid when there is already an existing round' do
      second_round = build(:assessment_round)
      second_round.valid?
      expect(second_round.errors.messages[:department]).to include('There can only be one open round/department.')
    end

    skip 'is valid when there is a closed exisiting round' do
    end
  end

  describe '#due_date_after_start_date' do
    it 'is invalid when the due date is on the start date' do
      round = build(:assessment_round, due_date: Date.today)
      round.valid?
      expect(round.errors.messages[:due_date]).to include('Due date needs to be after the start date.')
    end

    it 'is invalid when the due date is before the start date' do
      round = build(:assessment_round, due_date: Date.yesterday)
      round.valid?
      expect(round.errors.messages[:due_date]).to include('Due date needs to be after the start date.')
    end
  end

  describe '#dates_are_not_in_the_past' do
    it 'is invalid when the start date is in the past' do
      round = build(:assessment_round, start_date: Date.yesterday)
      round.valid?
      expect(round.errors.messages[:start_date]).to include('Start date can not be in the past.')
    end
  end

  describe '#move_to_review!' do
    it 'moves the round to review' do
      assessment_round.move_to_review!
      expect(assessment_round.state).to eq('in_review')
    end

    it 'does not move the round to review when it was already closed' do
      assessment_round.state = :closed
      assessment_round.move_to_review!

      expect(assessment_round.state).to eq('closed')
    end
  end

  describe '#close!' do
    it 'closes the round' do
      assessment_round.state = :in_review
      assessment_round.close!

      expect(assessment_round.state).to eq('closed')
    end

    it 'does not close the round when it is still open' do
      assessment_round.close!

      expect(assessment_round.state).to eq('open')
    end
  end
end
