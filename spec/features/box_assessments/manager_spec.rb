# frozen_string_literal: true

require 'rails_helper'

describe 'Box assessments as a manager', :js do
  let(:user) { create(:user, email: 'michael@gitlab.com', bamboo_id: 1) }
  let!(:assessment_round) { create(:assessment_round, functional_lead_id: 1) }

  before do
    login_as(user, scope: :user)
  end

  context 'when manager of an open assessment round' do
    context 'when on the root page' do
      it 'shows the button to start the assessment round' do
        visit '/'
        expect(page).to have_text('Assess your team members in Finance')
      end
    end

    context 'when on the 9 box page' do
      before do
        visit "/assessment_rounds/#{assessment_round.id}/assessments"
      end

      it 'has the direct reports checkbox enabled by default' do
        expect(page.find("input#reportsCheckbox")).to be_checked
        expect(page).to have_selector('.list-group-item', count: 1)
      end

      it 'can toggle the direct reports checkbox' do
        find("input#reportsCheckbox").set(false)
        expect(page).to have_selector('.list-group-item', count: 3)
      end

      it 'can filter on roles' do
        find('.js-roles select').select('Accountant')
        expect(page).to have_selector('.list-group-item', count: 1)
        expect(page).to have_selector('.bi-x-circle')
      end
    end
  end

  context 'when manager of a non-open assessment round' do
    let(:user) { create(:user, email: 'darryl@gitlab.com', bamboo_id: 7) }

    context 'when on the root page' do
      it 'shows the button to start the assessment round' do
        visit '/'
        expect(page).to have_text("You don't need to make any assessments at this moment.")
      end
    end
  end
end
