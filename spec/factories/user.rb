# frozen_string_literal: true

FactoryBot.define do
  sequence :email do |n|
    "jane#{n}@gitlab.com"
  end
end

FactoryBot.define do
  factory :user do
    email
    password { 'Passw0rd123e' }
  end
end
