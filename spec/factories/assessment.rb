# frozen_string_literal: true

FactoryBot.define do
  factory :assessment do
    motivation { 'Well deserved' }
    score_performance { 2 }
    score_potential { 2 }
    user factory: :user
    team_member_bamboo_id { 5 }
    assessment_round factory: :assessment_round
  end
end
