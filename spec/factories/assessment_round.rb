# frozen_string_literal: true

# == Schema Information
#
# Table name: assessment_rounds
#
#  id         :bigint           not null, primary key
#  department :string
#  due_date   :date
#  start_date :date
#  state      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint           not null
#
# Indexes
#
#  index_assessment_rounds_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :assessment_round do
    department { "Finance" }
    start_date { Date.today }
    due_date { Date.today + 7.days }
    state { 0 }
    user factory: :user
    functional_lead_id { 413 }
  end
end
