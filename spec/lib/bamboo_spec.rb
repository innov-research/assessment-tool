# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Bamboo do
  let(:team_members_file) { JSON.parse(File.read(File.join(__dir__, '../fixtures/team_members.json'))) }

  let(:bamboo_employee_mock) { instance_double(Bamboozled::API::Employee, add_table_row: true) }
  let(:bamboo_mock) do
    instance_double(Bamboozled::Base,
                    report: bamboo_report_mock,
                    employee: bamboo_employee_mock)
  end
  let(:bamboo) { described_class.new }

  before do
    allow(Date).to receive(:today).and_return(Date.parse('2020-10-03'))
  end

  describe '#find_team_member' do
    it 'finds the team member correctly' do
      expect(described_class.new.find_team_member(email: 'michael@gitlab.com')).to eq(team_members_file[0])
    end
  end

  describe '#find_team_member_by_id' do
    it 'finds the team member correctly' do
      expect(described_class.new.find_team_member_by_id(1)).to eq(team_members_file[0])
    end
  end

  describe '#find_team_members_by_department' do
    it 'returns the team members correctly' do
      expect(described_class.new.find_team_members_by_department('Direct Sales')).to eq(team_members_file[1..2])
    end
  end

  describe '#find_team_members_by_division' do
    it 'returns the team members correctly' do
      expect(described_class.new.find_team_members_by_division('Sales')).to eq(team_members_file[1..2])
    end
  end

  describe '#active_team_members' do
    it 'returns the team members correctly and does not include team members in the future' do
      expect(described_class.new.active_team_members).to eq(team_members_file[0...-1])
    end
  end

  describe '#all_reports' do
    it 'returns the team members correctly' do
      expect(described_class.new.all_reports(1).pluck('workEmail').sort).to eq(team_members_file[1...-1].pluck('workEmail').sort)
    end
  end

  describe '#direct_reports' do
    it 'returns the team members correctly' do
      expect(described_class.new.direct_reports(4)).to eq(team_members_file[4..5])
    end
  end

  describe '#reports_in_departments' do
    it 'returns the team members correctly' do
      expect(described_class.new.reports_in_departments(['People Success'], 'toby@gitlab.com')).to eq([team_members_file[9]])
    end
  end

  describe '#reports_in_divisions' do
    it 'returns the team members correctly' do
      expect(described_class.new.reports_in_divisions(['G&A'], 'toby@gitlab.com').pluck('id')).to eq(%w[4 5 6 10])
    end
  end

  describe '#add_rating_details' do
    before do
      allow(ENV).to receive(:[]).and_call_original
      allow(ENV).to receive(:[]).with('DRY_RUN').and_return(nil)
    end

    it 'adds the ratings successfully' do
      described_class.new.add_rating_details(1, 'Developing', 'High Potential')
      expect(bamboo_employee_mock).to have_received(:add_table_row).with(1, 'customPerformanceandPotentialTable', customEffectiveDate4: '2020-10-03', customPerformanceFactor: 'Developing', customPotentialFactor: 'High Potential')
    end
  end
end
