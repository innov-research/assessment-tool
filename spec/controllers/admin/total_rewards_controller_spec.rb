# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Admin::TotalRewardsController, type: :controller do
  before do
    request.env['devise.mapping'] = Devise.mappings[:user]
  end

  describe 'GET index' do
    before do
      sign_in(user)
    end

    context 'when it is an admin user' do
      let(:user) { create(:user, email: 'angela@gitlab.com', bamboo_id: 4, role: :admin) }

      it 'loads the index page' do
        get :index
        expect(response).to render_template('index')
      end
    end

    context 'when it is a total rewards user' do
      let(:user) { create(:user, email: 'angela@gitlab.com', bamboo_id: 4, role: :total_rewards) }

      it 'loads the index page' do
        get :index
        expect(response).to render_template('index')
      end
    end

    context 'when not an admin user' do
      let(:user) { create(:user, email: 'angela@gitlab.com', bamboo_id: 4) }

      it 'loads the index page' do
        get :index
        expect(response).to redirect_to(root_path)
      end
    end
  end
end
