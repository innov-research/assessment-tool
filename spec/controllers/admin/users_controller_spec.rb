# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Admin::UsersController, type: :controller do
  let(:meta_fields_json_file) { JSON.parse(File.read(File.join(__dir__, '../../fixtures/meta.json'))) }
  let(:bamboo_meta_mock) { instance_double(Bamboozled::API::Meta, lists: meta_fields_json_file) }
  let(:bamboo_mock) { instance_double(Bamboozled::Base, report: bamboo_report_mock, meta: bamboo_meta_mock) }

  before do
    request.env['devise.mapping'] = Devise.mappings[:user]
  end

  describe 'GET index' do
    before do
      sign_in(user)
    end

    context 'when it is an admin user' do
      let(:user) { create(:user, email: 'angela@gitlab.com', bamboo_id: 4, role: :admin) }

      it 'loads the index page' do
        get :index
        expect(response).to render_template('index')
      end
    end

    context 'when not an admin user' do
      let(:user) { create(:user, email: 'angela@gitlab.com', bamboo_id: 4) }

      it 'loads the index page' do
        get :index
        expect(response).to redirect_to(root_path)
      end
    end
  end
end
