# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AssessmentRoundsController, type: :controller do
  let(:assessment_round) { create(:assessment_round) }

  before do
    allow(ENV).to receive(:[]).and_call_original
    # This will envoke the code as close as possible to trying to sync to BambooHR
    # without actually syncing it to BambooHR.
    allow(ENV).to receive(:[]).with('DRY_RUN').and_return(true)
    allow(ENV).to receive(:[]).with('SYNC_TO_BAMBOO').and_return(true)

    sign_in(user)
  end

  context 'when the user is not a people business partner' do
    let(:user) { create(:user, email: 'angela@gitlab.com', bamboo_id: 4) }

    describe 'GET new' do
      it 'returns the root path' do
        get :new
        expect(response).to redirect_to(root_path)
      end
    end

    describe 'POST create' do
      it 'returns the root path' do
        post :create, params: { assessment_round: { department: 'People Success', due_date: Date.today + 7.days, start_date: Date.today } }
        expect(response).to redirect_to(root_path)
      end
    end

    describe 'GET show' do
      it 'returns the root path' do
        get :show, params: { id: assessment_round.id }
        expect(response).to redirect_to(root_path)
      end
    end

    describe 'POST toggle_review_state' do
      it 'returns the root path' do
        post :toggle_review_state, params: { id: assessment_round.id }
        expect(response).to redirect_to(root_path)
      end
    end

    describe 'GET review' do
      it 'returns the root path' do
        get :review, params: { id: assessment_round.id }
        expect(response).to redirect_to(root_path)
      end
    end

    describe 'POST close' do
      it 'returns the root path' do
        post :close, params: { id: assessment_round.id }
        expect(response).to redirect_to(root_path)
      end
    end

    describe 'POST sync' do
      it 'returns the root path' do
        post :sync, params: { id: assessment_round.id }
        expect(response).to redirect_to(root_path)
      end
    end

    describe 'GET download' do
      it 'returns the root path' do
        get :download, params: { id: assessment_round.id }
        expect(response).to redirect_to(root_path)
      end
    end

    describe 'GET edit' do
      it 'returns the root path' do
        get :edit, params: { id: assessment_round.id }
        expect(response).to redirect_to(root_path)
      end
    end

    describe 'POST update' do
      it 'returns the root path' do
        post :update, params: { id: assessment_round.id, assessment_round: { due_date: Date.today + 7.days, start_date: Date.today } }
        expect(response).to redirect_to(root_path)
      end
    end
  end

  context 'when the user is a People Business partner' do
    let(:user) { create(:user, email: 'jarmendariz@gitlab.com', bamboo_id: 8, role: :people_business_partner, departments: ["Accounting", "People Success", "Finance"]) }
    let(:assessment_round) { create(:assessment_round, user: user) }

    describe 'GET new' do
      it 'returns the form' do
        get :new
        expect(assigns(:assessment_round)).to be_a_new(AssessmentRound)
        expect(response).to render_template("new")
      end
    end

    describe 'POST create' do
      it 'creates an assessment round' do
        assessment_round_count = AssessmentRound.count
        post :create, params: { assessment_round: { department: 'People Success', due_date: Date.today + 7.days, start_date: Date.today } }
        expect(assigns(:assessment_round)).to be_a(AssessmentRound)
        expect(assigns(:assessment_round)).to be_persisted
        expect(response).to redirect_to(assessment_round_path(AssessmentRound.last))
        new_assessment_round_count = AssessmentRound.count
        expect(assessment_round_count + 1).to eq(new_assessment_round_count)
      end

      context 'when there is already an assessment round for this department' do
        it 'does not create a new assessment round' do
          create(:assessment_round, department: 'People Success')
          post :create, params: { assessment_round: { department: 'People Success', due_date: Date.today + 7.days, start_date: Date.today } }
          expect(assigns(:assessment_round)).not_to be_persisted
          expect(response).to render_template('new')
        end
      end
    end

    describe 'GET show' do
      it 'renders the show page' do
        get :show, params: { id: assessment_round.id }
        expect(assigns(:assessment_round)).to be_a(AssessmentRound)
        expect(assigns(:assessment_round)).to be_persisted
        expect(assigns(:people_managers)).to be_a(Array)
        expect(response).to render_template('show')
      end
    end

    describe 'POST toggle_review_state' do
      it 'moves the round to in review' do
        post :toggle_review_state, params: { id: assessment_round.id }
        expect(assigns(:assessment_round)).to be_a(AssessmentRound)
        expect(assigns(:assessment_round)).to be_persisted
        assessment_round.reload
        expect(assessment_round.state).to eq('in_review')
        expect(response).to redirect_to(assessment_round_path)
      end
    end

    describe 'GET review' do
      it 'returns the review form' do
        assessment_round.move_to_review!
        get :review, params: { id: assessment_round.id }
        expect(assigns(:assessment_round)).to be_a(AssessmentRound)
        expect(assigns(:assessment_round)).to be_persisted
        expect(response).to render_template('review')
      end
    end

    describe 'POST close' do
      it 'closes the round' do
        assessment_round.update(state: :in_review)
        post :close, params: { id: assessment_round.id }
        expect(assigns(:assessment_round)).to be_a(AssessmentRound)
        expect(assigns(:assessment_round)).to be_persisted
        assessment_round.reload
        expect(assessment_round.state).to eq('closed')
        expect(response).to redirect_to(assessment_round_path)
      end
    end

    describe 'POST sync' do
      it 'syncs the assessments of round' do
        assessment_round.update(state: :closed)
        post :sync, params: { id: assessment_round.id }
        expect(assigns(:assessment_round)).to be_a(AssessmentRound)
        expect(assigns(:assessment_round)).to be_persisted
        assessment_round.reload
        expect(assessment_round.state).to eq('synced')
        expect(response).to redirect_to(assessment_round_path)
      end
    end

    describe 'GET edit' do
      it 'returns the edit page' do
        get :edit, params: { id: assessment_round.id }
        expect(assigns(:assessment_round)).to be_a(AssessmentRound)
        expect(assigns(:assessment_round)).to be_persisted
        expect(response).to render_template('edit')
      end
    end

    describe 'POST update' do
      it 'updates the assessment round' do
        post :update, params: { id: assessment_round.id, assessment_round: { promotion_planning: true } }
        assessment_round.reload
        expect(assessment_round.promotion_planning).to eq(true)
      end
    end
  end
end
