# frozen_string_literal: true

require 'rails_helper'

describe AssessmentRoundPolicy do
  subject { described_class.new(user, assessment_round) }

  let(:user) { create(:user) }
  let(:assessment_round) { create(:assessment_round) }

  context 'when the user is not logged in' do
    let(:user) { nil }

    it { is_expected.to forbid_actions([:new, :create, :show, :toggle_review_state, :review, :box_review, :close, :sync, :download, :edit, :update, :closed_view]) }
  end

  context 'when it is the team member being assessed' do
    let(:user) { create(:user, email: 'kevin@gitlab.com', bamboo_id: 5) }

    it { is_expected.to forbid_actions([:new, :create, :show, :toggle_review_state, :review, :box_review, :close, :sync, :download, :edit, :update, :closed_view]) }
  end

  context 'when the user is an individual contributor' do
    let(:user) { create(:user, email: 'oscar@gitlab.com', bamboo_id: 6) }

    it { is_expected.to forbid_actions([:new, :create, :show, :toggle_review_state, :review, :box_review, :close, :sync, :download, :edit, :update, :closed_view]) }
  end

  context 'when the user is a manager' do
    let(:user) { create(:user, email: 'angela@gitlab.com', bamboo_id: 4) }

    it { is_expected.to forbid_actions([:new, :create, :show, :toggle_review_state, :review, :box_review, :close, :sync, :download, :edit, :update, :closed_view]) }
  end

  context 'when the user is a director' do
    let(:user) { create(:user, email: 'angela@gitlab.com', bamboo_id: 4) }

    before do
      team_members[3]['jobTitle'] = 'Director, Accountant'
    end

    it { is_expected.to forbid_actions([:new, :create, :show, :toggle_review_state, :review, :box_review, :close, :sync, :download, :edit, :update, :closed_view]) }
  end

  context 'when the user is the indirect manager' do
    let(:user) { create(:user, email: 'michael@gitlab.com', bamboo_id: 1) }

    it { is_expected.to forbid_actions([:new, :create, :show, :toggle_review_state, :review, :box_review, :close, :sync, :download, :edit, :update, :closed_view]) }
  end

  context 'when the user is a people business partner' do
    let(:user) { create(:user, email: 'jarmendariz@gitlab.com', bamboo_id: 8, role: :people_business_partner, departments: ['Finance']) }

    it { is_expected.to permit_actions([:new, :create, :show, :toggle_review_state, :review, :box_review, :close, :sync, :download, :edit, :update, :closed_view]) }
  end

  context 'when the user is a people business partner for a different department' do
    let(:user) { create(:user, email: 'glucchesi@gitlab.com', bamboo_id: 9, role: :people_business_partner, departments: ['Development']) }

    it { is_expected.to forbid_actions([:new, :create, :show, :toggle_review_state, :review, :box_review, :close, :sync, :download, :edit, :update, :closed_view]) }
  end
end
