# frozen_string_literal: true

require 'rails_helper'

describe UserAssessmentPolicy do
  subject { described_class.new(user, assessment_for) }

  let(:user) { User.create }
  let(:assessment_for) { team_members[4] }

  context 'when the user is not logged in' do
    let(:user) { nil }

    it { is_expected.to forbid_action(:index) }
  end

  context 'when it is the team member being assessed' do
    let(:user) { User.create(email: 'kevin@gitlab.com', bamboo_id: 5) }

    it { is_expected.to forbid_action(:index) }
  end

  context 'when the user is an individual contributor' do
    let(:user) { User.create(email: 'oscar@gitlab.com', bamboo_id: 6) }

    it { is_expected.to forbid_action(:index) }
  end

  context 'when the user is the manager' do
    let(:user) { User.create(email: 'angela@gitlab.com', bamboo_id: 4) }

    it { is_expected.to permit_action(:index) }
  end

  context 'when the user is the indirect manager' do
    let(:user) { User.create(email: 'michael@gitlab.com', bamboo_id: 1) }

    it { is_expected.to permit_action(:index) }
  end

  context 'when the user is an unrelated manager' do
    let(:user) { User.create(email: 'darryl@gitlab.com', bamboo_id: 7) }

    it { is_expected.to forbid_action(:index) }
  end
end
