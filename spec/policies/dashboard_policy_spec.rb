# frozen_string_literal: true

require 'rails_helper'

describe DashboardPolicy do
  subject { described_class.new(user, nil) }

  let(:user) { create(:user) }

  context 'when the user is not logged in' do
    let(:user) { nil }

    it { is_expected.to forbid_actions([:index]) }
  end

  context 'when it is the team member being assessed' do
    let(:user) { create(:user, email: 'kevin@gitlab.com', bamboo_id: 5) }

    it { is_expected.to forbid_actions([:index]) }
  end

  context 'when the user is an individual contributor' do
    let(:user) { create(:user, email: 'oscar@gitlab.com', bamboo_id: 6) }

    it { is_expected.to forbid_actions([:index]) }
  end

  context 'when the user is a manager' do
    let(:user) { create(:user, email: 'angela@gitlab.com', bamboo_id: 4) }

    it { is_expected.to forbid_actions([:index]) }
  end

  context 'when the user is a director' do
    let(:user) { create(:user, email: 'angela@gitlab.com', bamboo_id: 4) }

    before do
      team_members[3]['jobTitle'] = 'Director, Accountant'
    end

    it { is_expected.to forbid_actions([:index]) }
  end

  context 'when the user is the indirect manager' do
    let(:user) { create(:user, email: 'michael@gitlab.com', bamboo_id: 1) }

    it { is_expected.to forbid_actions([:index]) }
  end

  context 'when the user is a people business partner' do
    let(:user) { create(:user, email: 'jarmendariz@gitlab.com', bamboo_id: 8, role: :people_business_partner) }

    it { is_expected.to permit_action(:index) }
  end
end
