# frozen_string_literal: true

require 'rails_helper'

describe AssessmentPolicy do
  subject { described_class.new(user, policy_topic) }

  let(:user) { create(:user) }
  let(:policy_topic) { create(:assessment) }

  context 'when the user is not logged in' do
    let(:user) { nil }

    it { is_expected.to forbid_actions([:index, :table_view, :new, :create, :update_box_view]) }
  end

  context 'when it is the team member being assessed' do
    let(:user) { create(:user, email: 'kevin@gitlab.com', bamboo_id: 5) }

    it { is_expected.to forbid_actions([:index, :table_view, :new, :create, :update_box_view]) }
  end

  context 'when the user is an individual contributor' do
    let(:user) { create(:user, email: 'oscar@gitlab.com', bamboo_id: 6) }

    it { is_expected.to forbid_actions([:index, :table_view, :new, :create, :update_box_view]) }
  end

  context 'when the user is the manager' do
    let(:user) { create(:user, email: 'angela@gitlab.com', bamboo_id: 4) }

    it { is_expected.to permit_actions([:index, :new, :create, :update_box_view]) }

    context 'when we check the assessment for' do
      let(:policy_topic) { team_members[4] }

      it { is_expected.to permit_actions([:table_view]) }
    end
  end

  context 'when the user is a director' do
    let(:user) { create(:user, email: 'angela@gitlab.com', bamboo_id: 4) }

    before do
      team_members[3]['jobTitle'] = 'Director, Accountant'
    end

    it { is_expected.to permit_actions([:index, :new, :create, :update_box_view]) }

    context 'when we check the assessment for' do
      let(:policy_topic) { team_members[4] }

      it { is_expected.to permit_actions([:table_view]) }
    end
  end

  context 'when the user is the indirect manager' do
    let(:user) { create(:user, email: 'michael@gitlab.com', bamboo_id: 1) }

    it { is_expected.to permit_actions([:index, :new, :create, :update_box_view]) }

    context 'when we check the assessment for' do
      let(:policy_topic) { user }

      it { is_expected.to permit_actions([:table_view]) }
    end
  end

  context 'when the user is an unrelated manager' do
    let(:user) { create(:user, email: 'darryl@gitlab.com', bamboo_id: 7) }

    it { is_expected.to permit_actions([:index, :table_view]) }
    it { is_expected.to forbid_actions([:new, :create, :update_box_view]) }
  end

  context 'when the user is a people business partner' do
    let(:user) { create(:user, email: 'jarmendariz@gitlab.com', bamboo_id: 8, role: :people_business_partner) }

    it { is_expected.to permit_actions([:index, :new, :create, :update_box_view]) }
  end
end
