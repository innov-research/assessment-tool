# frozen_string_literal: true

module BambooClientContext
  extend RSpec::SharedContext

  let(:team_members) { JSON.parse(File.read(File.join(__dir__, '../fixtures/team_members.json'))) }
  let(:bamboo_report_mock) { instance_double(Bamboozled::API::Report, find: team_members) }
  let(:bamboo_mock) { instance_double(Bamboozled::Base, report: bamboo_report_mock) }

  before do
    allow(Bamboozled).to receive(:client).and_return(bamboo_mock)
  end
end
